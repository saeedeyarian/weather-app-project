const input = document.querySelector (".main form input");
const button = document.querySelector(".main i");
const content = document.querySelector (".main");
const container = document.querySelector (".container");
const message = document.querySelector (".message")

const apiKey= "bfcb8a4d44b365b0ed134e62b7e0fab2";

button.addEventListener("click" , e => {  
    e.preventDefault();

    content.style.display = "none";
    const inputValue = input.value; 
    const url=`http://api.openweathermap.org/data/2.5/weather?q=${inputValue}&appid=${apiKey}&units=metric` 
    fetch(url)
    .then (Response => Response.json())
    .then ( data => {
        console.log(data)
        const {main,weather, sys,name} = data
        
        
        if(inputValue === `${name}`){
            const showWeather = `
            <i class="fa fa-reply"></i>
            <section class="top-data">
            <div>
            <h3>${main.temp}</h3>
            <p>${name} , ${sys.country}</p>
            </div>
            <div>
            <h3>${weather[0]["main"]}</h3>
            <p>${weather[0]["description"]}</p>
            </div>
            </section>
            <section class="bottom-data">
            <div>
            <p class="text">Feels like</p>
            <p class="value">${main.feels_like}</p>
            </div>
            <div>
            <p class="text">Humidity</p>
            <p class="value">${main.humidity}</p>
            </div>
            <div>
            <p class="text"> Pressure</p>
            <p class="value">${main.pressure}</p>
            </div>
            <div>
            <p class="text">Min temp</p>
            <p class="value">${main.temp_min}</p>
            </div>
            <div>
            <p class="text">Max temp</p>
            <p class="value">${main.temp_max}</p>
            </div>
            </section>
            `
            const div = document.createElement ("div");
            div.classList.add ("weather");
            div.innerHTML = showWeather;
            container.appendChild(div)
            
            
            const back = document.querySelector (".weather i")
            const show = document.querySelector (".weather")
            
            back.addEventListener("click", e => {

                show.style.display = "none";
                content.style.display = "flex";
                input.value = "";
            })    
        } if (inputValue !== `${name}`) {
            alert("please inter a valid city")
            content.style.display = "flex";
        }
        })
    })